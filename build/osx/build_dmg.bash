#!/bin/bash
#
# Tranquil Java Integrated Development Environment
#
# The GNU General Public License Version 3
#
# Copyright (C) 2021 Autumn Lamonte
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# This creates a DMG file.  It is expected to be run from
# tjide/build/osx.  It leaves behind the 'dmg' directory which can be
# removed.

VERSION=1.0beta
APPDIR="Tranquil Java.app"


if [ ! -f "$APPDIR"/Contents/MacOS/ptypipe ]; then
    echo "ptypipe binary not found.  Build it on a Mac first!"
    return -1;
fi

if [ ! -f ../../java/build/jar/tjide.jar ]; then
    echo "tjide.jar not found."
    return -1;
fi

mkdir dmg
cp -r "$APPDIR" dmg
cp ../../ChangeLog dmg/ChangeLog.txt
cp ../../java/build/jar/tjide.jar dmg/"$APPDIR"/Contents/MacOS/tjide.jar

genisoimage -V TJIDE -D -R -apple -no-pad -o tjide-${VERSION}.dmg dmg
