/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.demos;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

import gjexer.TApplication;
import gjexer.TEditColorThemeWindow;
import gjexer.TEditorWindow;
import gjexer.event.TMenuEvent;
import gjexer.menu.TMenu;
import gjexer.menu.TMenuItem;
import gjexer.menu.TSubMenu;
import gjexer.backend.Backend;
import gjexer.backend.SwingTerminal;

/**
 * The demo application itself.
 */
public class DemoApplication extends TApplication {

    /**
     * Translated strings.
     */
    private static final ResourceBundle i18n = ResourceBundle.getBundle(DemoApplication.class.getName());

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param input an InputStream connected to the remote user, or null for
     * System.in.  If System.in is used, then on non-Windows systems it will
     * be put in raw mode; shutdown() will (blindly!) put System.in in cooked
     * mode.  input is always converted to a Reader with UTF-8 encoding.
     * @param output an OutputStream connected to the remote user, or null
     * for System.out.  output is always converted to a Writer with UTF-8
     * encoding.
     * @throws UnsupportedEncodingException if an exception is thrown when
     * creating the InputStreamReader
     */
    public DemoApplication(final InputStream input,
        final OutputStream output) throws UnsupportedEncodingException {
        super(input, output);
        addAllWidgets();

        getBackend().setTitle(i18n.getString("applicationTitle"));
    }

    /**
     * Public constructor.
     *
     * @param input the InputStream underlying 'reader'.  Its available()
     * method is used to determine if reader.read() will block or not.
     * @param reader a Reader connected to the remote user.
     * @param writer a PrintWriter connected to the remote user.
     * @param setRawMode if true, set System.in into raw mode with stty.
     * This should in general not be used.  It is here solely for Demo3,
     * which uses System.in.
     * @throws IllegalArgumentException if input, reader, or writer are null.
     */
    public DemoApplication(final InputStream input, final Reader reader,
        final PrintWriter writer, final boolean setRawMode) {
        super(input, reader, writer, setRawMode);
        addAllWidgets();

        getBackend().setTitle(i18n.getString("applicationTitle"));
    }

    /**
     * Public constructor.
     *
     * @param input the InputStream underlying 'reader'.  Its available()
     * method is used to determine if reader.read() will block or not.
     * @param reader a Reader connected to the remote user.
     * @param writer a PrintWriter connected to the remote user.
     * @throws IllegalArgumentException if input, reader, or writer are null.
     */
    public DemoApplication(final InputStream input, final Reader reader,
        final PrintWriter writer) {

        this(input, reader, writer, false);
    }

    /**
     * Public constructor.
     *
     * @param backend a Backend that is already ready to go.
     */
    public DemoApplication(final Backend backend) {
        super(backend);

        addAllWidgets();
    }

    /**
     * Public constructor.
     *
     * @param backendType one of the TApplication.BackendType values
     * @throws Exception if TApplication can't instantiate the Backend.
     */
    public DemoApplication(final BackendType backendType) throws Exception {
        // For the Swing demo, use an initial size of 82x28 so that a
        // terminal window precisely fits the window.
        super(backendType, (backendType == BackendType.SWING ? 82 : -1),
            (backendType == BackendType.SWING ? 28 : -1), 20);
        addAllWidgets();
        getBackend().setTitle(i18n.getString("applicationTitle"));
    }

    // ------------------------------------------------------------------------
    // TApplication -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Handle menu events.
     *
     * @param menu menu event
     * @return if true, the event was processed and should not be passed onto
     * a window
     */
    @Override
    public boolean onMenu(final TMenuEvent menu) {

        if (menu.getId() == 3000) {
            // Bigger +2
            assert (getScreen() instanceof SwingTerminal);
            SwingTerminal terminal = (SwingTerminal) getScreen();
            terminal.setFontSize(terminal.getFontSize() + 2);
            return true;
        }
        if (menu.getId() == 3001) {
            // Smaller -2
            assert (getScreen() instanceof SwingTerminal);
            SwingTerminal terminal = (SwingTerminal) getScreen();
            terminal.setFontSize(terminal.getFontSize() - 2);
            return true;
        }

        if (menu.getId() == 2050) {
            new TEditColorThemeWindow(this);
            return true;
        }

        if (menu.getId() == TMenu.MID_OPEN_FILE) {
            try {
                String filename = fileOpenBox(".");
                 if (filename != null) {
                     try {
                         new TEditorWindow(this, new File(filename));
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return super.onMenu(menu);
    }

    // ------------------------------------------------------------------------
    // DemoApplication --------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Add all the widgets of the demo.
     */
    private void addAllWidgets() {
        new DemoMainWindow(this);

        // Add the menus
        addToolMenu();
        addFileMenu();
        addEditMenu();

        TMenu demoMenu = addMenu(i18n.getString("demo"));
        TMenuItem item = demoMenu.addItem(2000, i18n.getString("checkable"));
        item.setCheckable(true);
        item = demoMenu.addItem(2001, i18n.getString("disabled"));
        item.setEnabled(false);
        item = demoMenu.addItem(2002, i18n.getString("normal"));
        TSubMenu subMenu = demoMenu.addSubMenu(i18n.getString("subMenu"));
        item = demoMenu.addItem(2010, i18n.getString("normal"));
        item = demoMenu.addItem(2050, i18n.getString("colors"));

        item = subMenu.addItem(2000, i18n.getString("checkableSub"));
        item.setCheckable(true);
        item = subMenu.addItem(2001, i18n.getString("disabledSub"));
        item.setEnabled(false);
        item = subMenu.addItem(2002, i18n.getString("normalSub"));

        subMenu = subMenu.addSubMenu(i18n.getString("subMenu"));
        item = subMenu.addItem(2000, i18n.getString("checkableSub"));
        item.setCheckable(true);
        item = subMenu.addItem(2001, i18n.getString("disabledSub"));
        item.setEnabled(false);
        item = subMenu.addItem(2002, i18n.getString("normalSub"));

        if (getScreen() instanceof SwingTerminal) {
            TMenu swingMenu = addMenu(i18n.getString("swing"));
            item = swingMenu.addItem(3000, i18n.getString("bigger"));
            item = swingMenu.addItem(3001, i18n.getString("smaller"));
        }

        addTableMenu();
        addWindowMenu();
        addHelpMenu();
    }

}
