/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.backend;

import java.awt.Insets;

/**
 * SwingSessionInfo provides a session implementation with a callback into
 * Swing to support queryWindowSize().  The username is blank, language is
 * "en_US", with a 80x25 text window.
 */
public class SwingSessionInfo implements SessionInfo {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The Swing JFrame or JComponent.
     */
    private SwingComponent swing;

    /**
     * The width of a text cell in pixels.
     */
    private int textWidth = 10;

    /**
     * The height of a text cell in pixels.
     */
    private int textHeight = 10;

    /**
     * User name.
     */
    private String username = "";

    /**
     * Language.
     */
    private String language = "en_US";

    /**
     * Text window width.
     */
    private int windowWidth = 80;

    /**
     * Text window height.
     */
    private int windowHeight = 25;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param swing the Swing JFrame or JComponent
     * @param textWidth the width of a cell in pixels
     * @param textHeight the height of a cell in pixels
     */
    public SwingSessionInfo(final SwingComponent swing, final int textWidth,
        final int textHeight) {

        this.swing      = swing;
        this.textWidth  = textWidth;
        this.textHeight = textHeight;
    }

    /**
     * Public constructor.
     *
     * @param swing the Swing JFrame or JComponent
     * @param textWidth the width of a cell in pixels
     * @param textHeight the height of a cell in pixels
     * @param width the number of columns
     * @param height the number of rows
     */
    public SwingSessionInfo(final SwingComponent swing, final int textWidth,
        final int textHeight, final int width, final int height) {

        this.swing              = swing;
        this.textWidth          = textWidth;
        this.textHeight         = textHeight;
        this.windowWidth        = width;
        this.windowHeight       = height;
    }

    // ------------------------------------------------------------------------
    // SessionInfo ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Username getter.
     *
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Username setter.
     *
     * @param username the value
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Language getter.
     *
     * @return the language
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Language setter.
     *
     * @param language the value
     */
    public void setLanguage(final String language) {
        this.language = language;
    }

    /**
     * Text window width getter.
     *
     * @return the window width
     */
    public int getWindowWidth() {
        return windowWidth;
    }

    /**
     * Text window height getter.
     *
     * @return the window height
     */
    public int getWindowHeight() {
        return windowHeight;
    }

    /**
     * Re-query the text window size.
     */
    public void queryWindowSize() {
        Insets insets = swing.getInsets();
        int width = swing.getWidth() - insets.left - insets.right;
        int height = swing.getHeight() - insets.top - insets.bottom;
        // In theory, if Java reported pixel-perfect dimensions, the
        // expressions above would precisely line up with the requested
        // window size from SwingComponent.setDimensions().  In practice,
        // there appears to be a small difference.  Add half a text cell in
        // both directions before the division to hopefully reach the same
        // result as setDimensions() was supposed to give us.
        width += (textWidth / 2);
        height += (textHeight / 2);
        windowWidth = width / textWidth;
        windowHeight = height / textHeight;

        /*
        System.err.printf("queryWindowSize(): frame %d %d window %d %d\n",
            swing.getWidth(), swing.getHeight(),
            windowWidth, windowHeight);
        */
    }

    // ------------------------------------------------------------------------
    // SwingSessionInfo -------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Set the dimensions of a single text cell.
     *
     * @param textWidth the width of a cell in pixels
     * @param textHeight the height of a cell in pixels
     */
    public void setTextCellDimensions(final int textWidth,
        final int textHeight) {

        this.textWidth  = textWidth;
        this.textHeight = textHeight;
    }

    /**
     * Getter for the underlying Swing component.
     *
     * @return the SwingComponent
     */
    public SwingComponent getSwingComponent() {
        return swing;
    }

}
