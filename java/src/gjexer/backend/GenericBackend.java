/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.backend;

import java.util.List;

import gjexer.event.TInputEvent;
import gjexer.event.TCommandEvent;
import static gjexer.TCommand.*;

/**
 * This abstract class provides a screen, keyboard, and mouse to
 * TApplication.  It also exposes session information as gleaned from lower
 * levels of the communication stack.
 */
public abstract class GenericBackend implements Backend {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The session information.
     */
    protected SessionInfo sessionInfo;

    /**
     * The screen to draw on.
     */
    protected Screen screen;

    /**
     * Input events are processed by this Terminal.
     */
    protected TerminalReader terminal;

    /**
     * By default, GenericBackend adds a cmAbort after it sees
     * cmBackendDisconnect, so that TApplication will exit when the user
     * closes the Swing window or disconnects the ECMA48 streams.  But
     * MultiBackend wraps multiple Backends, and needs to decide when to send
     * cmAbort differently.  Setting this to false is how it manages that.
     * Note package private access.
     */
    boolean abortOnDisconnect = true;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Backend ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Getter for sessionInfo.
     *
     * @return the SessionInfo
     */
    public final SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    /**
     * Getter for screen.
     *
     * @return the Screen
     */
    public final Screen getScreen() {
        return screen;
    }

    /**
     * Sync the logical screen to the physical device.
     */
    public void flushScreen() {
        screen.flushPhysical();
    }

    /**
     * Check if there are events in the queue.
     *
     * @return if true, getEvents() has something to return to the application
     */
    public boolean hasEvents() {
        return terminal.hasEvents();
    }

    /**
     * Get keyboard, mouse, and screen resize events.
     *
     * @param queue list to append new events to
     */
    public void getEvents(final List<TInputEvent> queue) {
        if (terminal.hasEvents()) {
            terminal.getEvents(queue);

            // This default backend assumes a single user, and if that user
            // becomes disconnected we should terminate the application.
            if ((queue.size() > 0) && (abortOnDisconnect == true)) {
                TInputEvent event = queue.get(queue.size() - 1);
                if (event instanceof TCommandEvent) {
                    TCommandEvent command = (TCommandEvent) event;
                    if (command.equals(cmBackendDisconnect)) {
                        queue.add(new TCommandEvent(cmAbort));
                    }
                }
            }
        }
    }

    /**
     * Close the I/O, restore the console, etc.
     */
    public void shutdown() {
        terminal.closeTerminal();
    }

    /**
     * Set the window title.
     *
     * @param title the new title
     */
    public void setTitle(final String title) {
        screen.setTitle(title);
    }

    /**
     * Set listener to a different Object.
     *
     * @param listener the new listening object that run() wakes up on new
     * input
     */
    public void setListener(final Object listener) {
        terminal.setListener(listener);
    }

    /**
     * Reload backend options from System properties.
     */
    public void reloadOptions() {
        terminal.reloadOptions();
    }

}
