/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.backend;

import java.util.List;

import gjexer.event.TInputEvent;

/**
 * This interface provides a screen, keyboard, and mouse to TApplication.  It
 * also exposes session information as gleaned from lower levels of the
 * communication stack.
 */
public interface Backend {

    /**
     * Get a SessionInfo, which exposes text width/height, language,
     * username, and other information from the communication stack.
     *
     * @return the SessionInfo
     */
    public SessionInfo getSessionInfo();

    /**
     * Get a Screen, which displays the text cells to the user.
     *
     * @return the Screen
     */
    public Screen getScreen();

    /**
     * Classes must provide an implementation that syncs the logical screen
     * to the physical device.
     */
    public void flushScreen();

    /**
     * Check if there are events in the queue.
     *
     * @return if true, getEvents() has something to return to the application
     */
    public boolean hasEvents();

    /**
     * Classes must provide an implementation to get keyboard, mouse, and
     * screen resize events.
     *
     * @param queue list to append new events to
     */
    public void getEvents(List<TInputEvent> queue);

    /**
     * Classes must provide an implementation that closes sockets, restores
     * console, etc.
     */
    public void shutdown();

    /**
     * Classes must provide an implementation that sets the window title.
     *
     * @param title the new title
     */
    public void setTitle(final String title);

    /**
     * Set listener to a different Object.
     *
     * @param listener the new listening object that run() wakes up on new
     * input
     */
    public void setListener(final Object listener);

    /**
     * Reload backend options from System properties.
     */
    public void reloadOptions();

}
