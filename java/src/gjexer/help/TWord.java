/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.help;

import gjexer.THelpWindow;
import gjexer.TWidget;
import gjexer.bits.CellAttributes;
import gjexer.bits.StringUtils;
import gjexer.event.TKeypressEvent;
import gjexer.event.TMouseEvent;
import static gjexer.TKeypress.*;

/**
 * TWord contains either a string to display or a clickable link.
 */
public class TWord extends TWidget {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The word(s) to display.
     */
    private String words;

    /**
     * Link to another Topic.
     */
    private Link link;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param words the words to display
     * @param link link to other topic, or null
     */
    public TWord(final String words, final Link link) {

        // TWord is created by THelpText before the TParagraph is belongs to
        // is created, so pass null as parent for now.
        super(null, 0, 0, StringUtils.width(words), 1);

        this.words = words;
        this.link = link;

        // Don't make text-only words "active".
        if (link == null) {
            setEnabled(false);
        }
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Handle mouse press events.
     *
     * @param mouse mouse button press event
     */
    @Override
    public void onMouseDown(final TMouseEvent mouse) {
        if (mouse.isMouse1()) {
            if (link != null) {
                ((THelpWindow) getWindow()).setHelpTopic(link.getTopic());
            }
        }
    }

    /**
     * Handle keystrokes.
     *
     * @param keypress keystroke event
     */
    @Override
    public void onKeypress(final TKeypressEvent keypress) {
        if (keypress.equals(kbEnter)) {
            if (link != null) {
                ((THelpWindow) getWindow()).setHelpTopic(link.getTopic());
            }
        }
    }

    // ------------------------------------------------------------------------
    // TWidget ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the words.
     */
    @Override
    public void draw() {
        CellAttributes color = getTheme().getColor("thelpwindow.text");
        if (link != null) {
            if (isAbsoluteActive()) {
                color = getTheme().getColor("thelpwindow.link.active");
            } else {
                color = getTheme().getColor("thelpwindow.link");
            }
        }
        putStringXY(0, 0, words, color);
    }

    // ------------------------------------------------------------------------
    // TWord ------------------------------------------------------------------
    // ------------------------------------------------------------------------

}
