/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.project;

import java.util.List;

import tjide.ui.TranquilApplication;

/**
 * RunnableTarget defines an API for determining if a Target can be run, and
 * if so what environment (arguments, memory, etc.) it needs.
 */
public interface RunnableTarget {

    /**
     * Determine whether or not this Target is runnable.
     *
     * @return true if this target can be executed via the runTarget() method
     */
    public boolean isRunnable();

    /**
     * Set whether or not this Target is runnable.
     *
     * @param runnable if true, this target can be executed via the
     * runTarget() method
     */
    public void setRunnable(final boolean runnable);

    /**
     * Execute this target.
     *
     * @param application the UI top-level application
     * @param project the project metadata
     * @throws NotRunnableException if this Target is not runnable.
     */
    public void runTarget(final TranquilApplication application,
        final Project project) throws NotRunnableException;

    /**
     * Get the arguments to runTarget().
     *
     * @return the arguments
     */
    public List<String> getRunArguments();

    /**
     * Set the arguments to runTarget().
     *
     * @param arguments the new arguments
     */
    public void setRunArguments(final List<String> arguments);

    /**
     * Get the JVM arguments to runTarget().
     *
     * @return the arguments
     */
    public List<String> getJvmRunArguments();

    /**
     * Set the JVM arguments to runTarget().
     *
     * @param arguments the new arguments
     */
    public void setJvmRunArguments(final List<String> arguments);

}
