/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.ui;

import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TField;
import gjexer.TWindow;

/**
 * NewWatchWindow is used to get a new target name to add to a project.
 */
public class NewWatchWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(NewWatchWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The name of the new watch.
     */
    private TField watchExpr;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.  The input box will be centered on screen.
     *
     * @param watchWindow the WatchWindow to add the watch to
     * @param add if true, make this an add watch dialog, else edit watch
     * @param expression the watch expression to edit if add is false
     */
    public NewWatchWindow(final WatchWindow watchWindow, final boolean add,
        final String expression) {

        super(watchWindow.getApplication(),
            i18n.getString(add ? "windowTitleAdd" : "windowTitleEdit"),
            1, 1, 60, 8, CENTERED | MODAL);

        addLabel(i18n.getString("watchExpression"), 2, 1,
            "twindow.background.modal",
            new TAction() {
                public void DO() {
                    activate(watchExpr);
                }
            });

        watchExpr = addField(2, 2, getWidth() - 6, false, "");
        if (!add) {
            watchExpr.setText(expression);
        }

        addButton(i18n.getString("okButton"), 19, getHeight() - 4,
            new TAction() {
                public void DO() {
                    if (getWatchExpr().length() > 0) {
                        if (add) {
                            watchWindow.addWatch(getWatchExpr());
                        } else {
                            watchWindow.updateWatch(getWatchExpr());
                        }
                    }
                    getApplication().closeWindow(NewWatchWindow.this);
                }
            });

        addButton(i18n.getString("cancelButton"), 31, getHeight() - 4,
            new TAction() {
                public void DO() {
                    getApplication().closeWindow(NewWatchWindow.this);
                }
            });

        activate(watchExpr);

    }

    // ------------------------------------------------------------------------
    // Event Handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // NewWatchInputBox -------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Retrieve the watch expr.
     *
     * @return the watch expr
     */
    public String getWatchExpr() {
        return watchExpr.getText().trim();
    }

}
