/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import gjexer.TAction;
import gjexer.TApplication;
import gjexer.TButton;
import gjexer.TCheckBox;
import gjexer.TComboBox;
import gjexer.TField;
import gjexer.TInputBox;
import gjexer.TWindow;
import gjexer.bits.CellAttributes;

import tjide.project.JarTarget;
import tjide.project.RunnableTarget;
import tjide.project.Target;

/**
 * This window is used to configure the per-target preferences.
 */
public class TargetOptionsWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static ResourceBundle i18n = ResourceBundle.getBundle(TargetOptionsWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The target being edited.
     */
    private Target target;

    /**
     * Whether or not this target is runnable.
     */
    private TCheckBox runnable;

    /**
     * Run arguments.
     */
    private TComboBox runArguments;

    /**
     * JVM run arguments.
     */
    private TComboBox jvmRunArguments;

    /**
     * Main class.
     */
    private TField mainClass;

    /**
     * Implementation-version.
     */
    private TField implementationVersion;

    /**
     * Include filters.
     */
    private TComboBox includeFilters;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param application TApplication that manages this window
     * @param target the target to edit
     */
    public TargetOptionsWindow(final TApplication application,
        final Target target) {

        super(application, i18n.getString("windowTitle"),
            1, 1, 70, 23, CENTERED | MODAL);

        this.target = target;

        runnable = addCheckBox(3, 2, i18n.getString("runnable"), false);
        addLabel(i18n.getString("runArguments"), 3, 4, "ttext", false);
        runArguments = addComboBox(22, 4, 25, new ArrayList<String>(), -1, 5,
            null);
        addLabel(i18n.getString("jvmRunArguments"), 3, 6, "ttext", false);
        jvmRunArguments = addComboBox(22, 6, 25, new ArrayList<String>(), -1, 5,
            null);

        addLabel(i18n.getString("mainClass"), 3, 11, "ttext", false);
        mainClass = addField(22, 11, 25, false);
        addLabel(i18n.getString("implementationVersion"), 3, 13, "ttext",
            false);
        implementationVersion = addField(22, 13, 25, false);
        addLabel(i18n.getString("includes"), 3, 15, "ttext", false);
        includeFilters = addComboBox(22, 15, 25, new ArrayList<String>(), -1, 5,
            null);

        if (target instanceof RunnableTarget) {
            RunnableTarget runnableTarget = (RunnableTarget) target;
            runnable.setChecked(runnableTarget.isRunnable());
            runArguments.setList(runnableTarget.getRunArguments());
            jvmRunArguments.setList(runnableTarget.getJvmRunArguments());

            addButton(i18n.getString("addRunArg"), 48, 4,
                new TAction() {
                    public void DO() {
                        TInputBox inputBox = getApplication().inputBox(i18n.
                            getString("addRunArgInputBoxTitle"),
                            i18n.getString("addRunArgInputBoxCaption"), "",
                            TInputBox.Type.OKCANCEL);

                        if (inputBox.getResult() == TInputBox.Result.OK) {
                            List<String> arguments = runArguments.getList();
                            arguments.add(inputBox.getText().trim());
                            runArguments.setList(arguments);
                        }
                    }
                }).setShadowColor(getTheme().getColor("ttext"));

            addButton(i18n.getString("delRunArg"), 55, 4,
                new TAction() {
                    public void DO() {
                        List<String> arguments = runArguments.getList();
                        arguments.remove(runArguments.getText());
                        runArguments.setList(arguments);
                    }
                }).setShadowColor(getTheme().getColor("ttext"));

            addButton(i18n.getString("addRunArg"), 48, 6,
                new TAction() {
                    public void DO() {
                        TInputBox inputBox = getApplication().inputBox(i18n.
                            getString("addJvmRunArgInputBoxTitle"),
                            i18n.getString("addJvmRunArgInputBoxCaption"), "",
                            TInputBox.Type.OKCANCEL);

                        if (inputBox.getResult() == TInputBox.Result.OK) {
                            List<String> arguments = jvmRunArguments.getList();
                            arguments.add(inputBox.getText().trim());
                            jvmRunArguments.setList(arguments);
                        }
                    }
                }).setShadowColor(getTheme().getColor("ttext"));

            addButton(i18n.getString("delRunArg"), 55, 6,
                new TAction() {
                    public void DO() {
                        List<String> arguments = jvmRunArguments.getList();
                        arguments.remove(jvmRunArguments.getText());
                        jvmRunArguments.setList(arguments);
                    }
                }).setShadowColor(getTheme().getColor("ttext"));

        } else {
            runnable.setEnabled(false);
            runArguments.setEnabled(false);
            jvmRunArguments.setEnabled(false);
        }

        if (target instanceof JarTarget) {
            JarTarget jarTarget = (JarTarget) target;
            mainClass.setText(jarTarget.getMainClass());
            implementationVersion.setText(jarTarget.getImplementationVersion());
            includeFilters.setList(jarTarget.getFilters());

            addButton(i18n.getString("addIncludeFilter"), 48, 15,
                new TAction() {
                    public void DO() {
                        TInputBox inputBox = getApplication().inputBox(i18n.
                            getString("addFilterInputBoxTitle"),
                            i18n.getString("addFilterInputBoxCaption"), "",
                            TInputBox.Type.OKCANCEL);

                        if (inputBox.getResult() == TInputBox.Result.OK) {
                            List<String> arguments = includeFilters.getList();
                            arguments.add(inputBox.getText().trim());
                            includeFilters.setList(arguments);
                        }
                    }
                }).setShadowColor(getTheme().getColor("ttext"));

            addButton(i18n.getString("delIncludeFilter"), 55, 15,
                new TAction() {
                    public void DO() {
                        List<String> arguments = includeFilters.getList();
                        arguments.remove(includeFilters.getText());
                        includeFilters.setList(arguments);
                    }
                }).setShadowColor(getTheme().getColor("ttext"));

        } else {
            mainClass.setEnabled(false);
            implementationVersion.setEnabled(false);
            includeFilters.setEnabled(false);
        }

        // Buttons
        addButton(i18n.getString("okButton"), 23, getHeight() - 4,
            new TAction() {
                public void DO() {
                    // Copy values from window to properties, close window.
                    copyOptions();
                    TargetOptionsWindow.this.close();
                }
            });

        TButton closeButton = addButton(i18n.getString("closeButton"),
            35, getHeight() - 4,
            new TAction() {
                public void DO() {
                    // Don't do anything, just close the window.
                    TargetOptionsWindow.this.close();
                }
            });

        // Save this for last: make the close button default action.
        activate(closeButton);
    }

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the exception message background.
     */
    @Override
    public void draw() {
        // Draw window and border.
        super.draw();

        CellAttributes boxColor = getTheme().getColor("ttext");

        // Java target options
        drawBox(2, 2, getWidth() - 2, 10, boxColor, boxColor);
        putStringXY(4, 2, i18n.getString("javaTargetOptions"), boxColor);

        // Java target options
        drawBox(2, 11, getWidth() - 2, 19, boxColor, boxColor);
        putStringXY(4, 11, i18n.getString("jarTargetOptions"), boxColor);

    }

    // ------------------------------------------------------------------------
    // TargetOptionsWindow ----------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Copy options from window fields to the project properties.
     */
    private void copyOptions() {
        if (target instanceof RunnableTarget) {
            RunnableTarget runnableTarget = (RunnableTarget) target;
            runnableTarget.setRunnable(runnable.isChecked());
            runnableTarget.setRunArguments(runArguments.getList());
            runnableTarget.setJvmRunArguments(jvmRunArguments.getList());
        }

        if (target instanceof JarTarget) {
            JarTarget jarTarget = (JarTarget) target;
            jarTarget.setMainClass(mainClass.getText().trim());
            jarTarget.setImplementationVersion(implementationVersion.
                getText().trim());
            jarTarget.setFilters(includeFilters.getList());
        }

    }

}
