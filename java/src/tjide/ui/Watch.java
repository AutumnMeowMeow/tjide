/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.ui;

import gjexer.TWidget;
import gjexer.bits.CellAttributes;

import tjide.debugger.Scope;

/**
 * Watch is a clickable item in the WatchWindow.
 */
public class Watch extends TWidget {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The watch expression.
     */
    private String expression;

    /**
     * The current watch value.
     */
    private String value = "";

    /**
     * Left column of the watch, for scrolling.  0 is the left-most column.
     */
    private int left = 0;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent the window this watch will be on
     * @param expression the watch expression
     */
    public Watch(final WatchWindow parent, final String expression) {
        super(parent, 0, 0, parent.getWidth() - 2, 1);

        setExpression(expression);
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TWidget ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the watch.
     */
    @Override
    public void draw() {
        CellAttributes color = null;
        if (isActive()) {
            color = getTheme().getColor("watchWindow.item.selected");
        } else {
            color = getTheme().getColor("watchWindow.item");
        }
        hLineXY(0, 0, getWindow().getWidth() - 2, ' ', color);

        String text = String.format("%s : %s", expression, value);
        if (left < text.length()) {
            int maxLength = getWindow().getWidth() - 2;
            if (left + maxLength > text.length()) {
                maxLength = text.length() - left;
            }
            putStringXY(0, 0, text.substring(left), color);
        }
    }

    // ------------------------------------------------------------------------
    // Watch ------------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the watch expression.
     *
     * @return the expression
     */
    public String getExpression() {
        return expression;
    }

    /**
     * Set the watch expression.
     *
     * @param expression the new expression
     */
    public void setExpression(final String expression) {
        this.expression = expression;
        updateValue();
    }

    /**
     * Get the watch value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Get the left column to render.
     *
     * @return the left column
     */
    public int getLeft() {
        return left;
    }

    /**
     * Set the left column to render.
     *
     * @param left the new left column
     */
    public void setLeft(final int left) {
        this.left = left;
        if (this.left < 0) {
            this.left = 0;
        }
    }

    /**
     * Get the display length.
     *
     * @return the number of cells to display this watch and value in
     */
    public int getDisplayLength() {
        return expression.length() + value.length() + 3;
    }

    /**
     * Update the watch value to reflect values at the current debug
     * execution location.
     */
    public void updateValue() {
        TranquilApplication app = (TranquilApplication) getApplication();

        Scope debugScope = app.getDebugScope();
        if (debugScope == null) {
            value = "";
            return;
        }
        value = debugScope.getValue(expression);
    }

}
