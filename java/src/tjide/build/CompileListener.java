/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.build;

import tjide.project.Target;

/**
 * CompileListener is a callback interface to monitor the progress of a
 * compile().
 */
public interface CompileListener {

    /**
     * Notify of a new warning message.  This will increment the warning
     * count.
     *
     * @param target the target that generated this message
     * @param line the line number of the message.  1-based: 0 or negative
     * means no relevant line number.
     * @param column the column number of the message.  1-based: 0 or
     * negative means no relevant column number.
     * @param message the message text
     */
    public void addCompileWarning(final Target target, final long line,
        final long column, final String message);

    /**
     * Notify of a new error message.  This will increment the error count.
     *
     * @param target the target that generated this message
     * @param line the line number of the message.  1-based: 0 or negative
     * means no relevant line number.
     * @param column the column number of the message.  1-based: 0 or
     * negative means no relevant column number.
     * @param message the message text
     */
    public void addCompileError(final Target target, final long line,
        final long column, final String message);

    /**
     * Notify a new error count.
     *
     * @param count the new count of errors
     */
    public void setCompileErrorCount(final int count);

    /**
     * Notify a new warning count.
     *
     * @param count the new count of warnings
     */
    public void setCompileWarningCount(final int count);

    /**
     * Notify a new line count.
     *
     * @param count the new count of lines
     */
    public void setCompileLineCount(final int count);

    /**
     * Notify a new available memory number.
     *
     * @param kbytes the number of kilobytes available
     */
    public void setCompileAvailableMemory(final long kbytes);

    /**
     * Notify of a compile beginning.
     */
    public void setCompileBegin();

    /**
     * Notify of a file beginning the compile.
     *
     * @param source the source filename being compiled
     * @param destination the destination filename being compiled
     */
    public void setCompileFile(final String source, final String destination);

    /**
     * Notify of a compile failure.
     *
     * @param completed if true, this is the final failure message
     */
    public void setCompileFailed(final boolean completed);

    /**
     * Notify of a compile success.
     *
     * @param completed if true, this is the final success message
     */
    public void setCompileSucceeded(final boolean completed);

    /**
     * Set the compile task callback interface, so that the CompileListener
     * can cancel if needed.
     *
     * @param compileTask the compile task
     */
    public void setCompileTask(final CompileTask compileTask);

}
