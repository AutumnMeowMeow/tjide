/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.debugger;

import com.sun.jdi.request.BreakpointRequest;

import tjide.project.FileTarget;

/**
 * Breakpoint represents a stopping point in a source file.
 */
public class Breakpoint {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The target for this breakpoint.
     */
    private FileTarget target;

    /**
     * The line number to break on.
     */
    private int line;

    /**
     * The breakpoint request as seen in the target VM.  Note package private
     * access.
     */
    BreakpointRequest breakpointRequest = null;

    /**
     * The number of times this breakpoint was passed in the current run.
     * Note package private access.
     */
    int passCount = 0;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param target the target for this breakpoint
     * @param line the line number to break on
     */
    public Breakpoint(final FileTarget target, final int line) {
        if (line <= 0) {
            throw new IllegalArgumentException("line number (" + line +
                ") must be positive");
        }

        this.target = target;
        this.line = line;
    }

    // ------------------------------------------------------------------------
    // Breakpoint -------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the target for this breakpoint.
     *
     * @return the target for this breakpoint
     */
    public FileTarget getTarget() {
        return target;
    }

    /**
     * Get the line number to break on.
     *
     * @return the line number to break on
     */
    public int getLine() {
        return line;
    }

    /**
     * Get the pass count.
     *
     * @return the number of times this breakpoint was passed in the current
     * run
     */
    public int getPassCount() {
        return passCount;
    }

}
