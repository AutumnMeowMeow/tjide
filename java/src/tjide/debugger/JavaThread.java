/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.debugger;

import com.sun.jdi.IncompatibleThreadStateException;
import com.sun.jdi.StackFrame;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.VMDisconnectedException;

/**
 * JavaThread represents a running thread in a Java virtual machine process.
 */
public class JavaThread extends DebugThread {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The debugger instance.
     */
    private JavaDebugger debugger = null;

    /**
     * The thread reference.  Note package private access.
     */
    ThreadReference thread;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Package private constructor.
     *
     * @param debugger the debugger
     * @param thread the thread reference from the JVM
     */
    JavaThread(final JavaDebugger debugger, final ThreadReference thread) {
        super(Long.toString(thread.uniqueID()), thread.name());

        this.debugger = debugger;
        this.thread = thread;
    }

    // ------------------------------------------------------------------------
    // DebugThread ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the current lowest-level scope of this thread.
     *
     * @return the thread's scope, or null if unavailable
     */
    @Override
    public Scope getScope() {
        try {
            StackFrame frame = thread.frame(0);
            return new JavaScope(debugger, frame.location(), thread, 0);
        } catch (IncompatibleThreadStateException e) {
            return null;
        } catch (VMDisconnectedException e) {
            return null;
        }
    }

}
