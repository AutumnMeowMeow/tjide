/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.debugger;

/**
 * DebugThread represents a running thread in the debugged process.
 */
public abstract class DebugThread {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * A unique ID to refer to this thread in the debugged process.
     */
    protected String id;

    /**
     * A human-readable name for this thread to expose in the UI.
     */
    protected String name;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Package private constructor.
     *
     * @param id the thread ID
     * @param name the thread name
     */
    DebugThread(final String id, final String name) {
        this.id = id;
        this.name = name;
    }

    // ------------------------------------------------------------------------
    // DebugThread ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the ID of this thread.
     *
     * @return the unique ID to refer to this thread in the debugged process
     */
    public String getId() {
        return id;
    }

    /**
     * The the name of this thread.
     *
     * @return a human-readable name for this thread to expose in the UI
     */
    public String getName() {
        return name;
    }

    /**
     * Get the current lowest-level scope of this thread.
     *
     * @return the thread's scope, or null if unavailable
     */
    public abstract Scope getScope();

}
